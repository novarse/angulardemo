import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {

  readonly URL = environment.serverUrl;

  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // hack (can't add content-type in upload-file-dialog.ts for some reason)
    const url = this.URL + req.url;

    return next.handle(req.clone({
      url,
      headers: this.getHeaders(req.headers)
    }));
  }

  getHeaders(headers: HttpHeaders) {
    headers = headers
      .set('Content-Type', 'application/json')
      .set('Access-Control-Allow-Origin', '*')
    ;
    return headers;
  }

}
