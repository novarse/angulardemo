import { Injectable } from '@angular/core';
import {DetailDto} from "src/app/dto/detail-dto";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  details: DetailDto = new DetailDto();
  submitted = false;
  agree = false;

}
