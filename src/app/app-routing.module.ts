import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "src/app/components/home/home.component";
import {NewsComponent} from "src/app/components/news/news.component";
import {MainLayoutComponent} from "src/app/components/main-layout/main-layout.component";
import {InputFormComponent} from "src/app/components/input-form/input-form.component";
import {HasAgreedGuard} from "src/app/guards/has-agreed.guard";
import {CanLeaveInputFormGuard} from "src/app/guards/can-leave-input-form.guard";
import {HasSubmittedGuard} from "src/app/guards/has-submitted.guard";

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    canActivateChild: [HasAgreedGuard],
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'

      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'input-form',
        canDeactivate: [CanLeaveInputFormGuard],
        component: InputFormComponent
      },
      {
        path: 'news',
        canActivate: [HasSubmittedGuard],
        component: NewsComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
