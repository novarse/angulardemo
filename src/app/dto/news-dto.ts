export class NewsDto {
  id?: number;
  messageShort?: string;
  message?: string;
  order?: number;
  startDate?: string;
  endDate?: string;
  alertFade?: boolean;

  constructor(
    id?: number,
    messageShort?: string,
    message?: string,
    order?: number,
    startDate?: string,
    endDate?: string,
    alertFade?: boolean
  )
  {
    this.id = id;
    this.messageShort = messageShort;
    this.message = message;
    this.order = order;
    this.startDate = startDate;
    this.endDate = endDate;
    this.alertFade = alertFade;
  }

}
