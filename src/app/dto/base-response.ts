export class BaseResponse {
  error: string | undefined;
  infoMsg: string | undefined;
}
