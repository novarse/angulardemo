export class DetailDto {
  givenName?: string;
  surname?: string;
  email?: string;
  birthdate?: string;
  role?: string;  // used to store gender

  constructor(givenName?: string,
              surname?: string,
              email?: string,
              birthdate?: string,
              role?: string) {
    this.givenName = givenName;
    this.surname = surname;
    this.email = email;
    this.birthdate = birthdate;
    this.role = role;
  }
}
