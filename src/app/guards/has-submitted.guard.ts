import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {DataService} from "src/app/services/data.service";

@Injectable({
  providedIn: 'root'
})
export class HasSubmittedGuard implements CanActivate {
  // @ts-ignore
  constructor(private dataService: DataService,
              private router: Router,
              private snackBar: MatSnackBar) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!this.dataService.submitted) {
      this.snackBar.open('Please submit input form', '', {duration: 3000, panelClass: 'red-snackbar'});
      this.router.navigate(['input-form']);
      return false;
    }
    return true;
  }

}
