import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {DataService} from "src/app/services/data.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {HomeComponent} from "src/app/components/home/home.component";

@Injectable({
  providedIn: 'root'
})
export class HasAgreedGuard implements CanActivateChild {
  constructor(private dataService: DataService,
              private router: Router,
              private snackBar: MatSnackBar) {
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (next.component !== HomeComponent && !this.dataService.agree) {
      this.snackBar.open('Please agree first', '', {duration: 3000, panelClass: 'red-snackbar'});
      this.router.navigate(['home']);
      return false;
    }
    return true;
  }

}
