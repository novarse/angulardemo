import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import {map, Observable} from 'rxjs';
import {InputFormComponent} from "src/app/components/input-form/input-form.component";
import {ContinueCancelDialogComponent} from "src/app/dialogs/continue-cancel-dialog/continue-cancel-dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Injectable({
  providedIn: 'root'
})
export class CanLeaveInputFormGuard implements CanDeactivate<unknown> {
  constructor(private dialog: MatDialog) {
  }

  canDeactivate(
    component: InputFormComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (component.form.dirty) {
      const dialogRef = this.dialog.open(ContinueCancelDialogComponent, {
        data: {
          title: 'Leaving form with unsaved changes',
          question: 'Do you want to continue?'
        }
      });

      return dialogRef.afterClosed()
        .pipe(
          map(val => val === '' ? false : true)
        );
    } else {
      return true;
    }
  }


}
