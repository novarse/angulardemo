import { Component, OnInit } from '@angular/core';
import {NewsDto} from "src/app/dto/news-dto";
import {BehaviorSubject, tap} from "rxjs";
import {HttpService} from "src/app/services/http.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {HtmlSnackbarComponent} from "src/app/components/html-snackbar/html-snackbar.component";

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  news?: NewsDto[];
  loaded = new BehaviorSubject<boolean>(false);

  constructor(private httpService: HttpService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.httpService.getNews()
      .pipe(
        tap(result => {
          this.news = result;
          this.loaded.next(true);
        })
      )
      .subscribe();
  }

  onMsgFeedback(msg: string) {
    this.snackBar.openFromComponent(HtmlSnackbarComponent, {
      duration: 10000, panelClass: 'html-snackbar',
      data: {
        html: msg
      }
    });
  }

}
