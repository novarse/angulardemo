import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NewsDto} from "src/app/dto/news-dto";

@Component({
  selector: '[app-news-line]',
  templateUrl: './news-line.component.html',
  styleUrls: ['./news-line.component.css']
})
export class NewsLineComponent implements OnInit {

  @Input()
  news = new NewsDto();

  @Output()
  msgFeedBack = new EventEmitter<string>();

  msg = '';

  constructor() {
  }

  ngOnInit(): void {
    this.msg = this.news.message ? this.news.message : '';
  }

  onFeedMsgBack(msg: string) {
    this.msg = msg;
    this.msgFeedBack.emit(this.msg);
  }
}
