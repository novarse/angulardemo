import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomValidators} from "src/app/utils/custom-validators";
import {MatSnackBar} from "@angular/material/snack-bar";
import {HttpService} from "src/app/services/http.service";
import {catchError, tap} from "rxjs";
import {DetailDto} from "src/app/dto/detail-dto";
import {UtilsService} from "src/app/utils/utils.service";
import {DataService} from "src/app/services/data.service";

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.css']
})
export class InputFormComponent implements OnInit {

  readonly EMAIL_PATTERN = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@'
    + '[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';

  form: FormGroup;

  readonly MIN_BIRTH_DATE = new Date(1950, 0, 2);
  readonly END_DATE = new Date(2006, 11, 32);

  genders = [
    {
      code: 'F',
      desc: 'Female'
    },
    {
      code: 'M',
      desc: 'Male'
    },
    {
      code: 'X',
      desc: 'Other'
    },
  ];

  constructor(private fb: FormBuilder,
              public snackBar: MatSnackBar,
              private http: HttpService,
              private utilsService: UtilsService,
              public dataService: DataService) {
    this.form = this.fb.group({
      givenName: [null, [Validators.required, CustomValidators.checkNameCharacters]],
      surname: [null, Validators.required],
      email: [null, [Validators.required, Validators.pattern(this.EMAIL_PATTERN)]],
      birthdate: [null, Validators.required],
      gender: [null, Validators.required]
    });
  }

  ngOnInit(): void {
    if (this.dataService.submitted) {
      this.form.patchValue(this.dataService.details);
      this.form.get('gender')?.setValue(this.dataService.details.role);
    }
  }

  onSubmit() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      let formBody = this.buildBody();
      this.http.postDetails(JSON.stringify(formBody))
        .pipe(
          tap(() => {
            this.snackBar.open('Submitted OK', '', {duration: 4000, panelClass: 'green-snackbar'});
            this.form.markAsPristine(); // so canDeactivate guard can properly check dirty status of form
            this.dataService.details = formBody;
            this.dataService.submitted = true;
          }),
          catchError(this.utilsService.handleError())
        )
        .subscribe();
    }
  }

  private buildBody(): DetailDto {
    return new DetailDto(
      this.form.get('givenName')?.value,
      this.form.get('surname')?.value,
      this.form.get('email')?.value,
      (new Date(this.form.get('birthdate')?.value)).toJSON(),
      this.form.get('gender')?.value
    );
  }

  onReset() {
    this.form.reset();
    this.dataService.submitted = false;
  }

}
