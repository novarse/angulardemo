import {Component, Inject, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-html-snackbar',
  templateUrl: './html-snackbar.component.html',
  styleUrls: ['./html-snackbar.component.css']
})
export class HtmlSnackbarComponent implements OnInit {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any,
              public snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
  }

}
