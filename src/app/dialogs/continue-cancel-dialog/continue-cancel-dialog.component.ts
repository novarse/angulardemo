import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'app-continue-cancel-dialog',
  templateUrl: './continue-cancel-dialog.component.html',
  styleUrls: ['./continue-cancel-dialog.component.css']
})
export class ContinueCancelDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

}
