import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { InputFormComponent } from './components/input-form/input-form.component';
import { NewsComponent } from './components/news/news.component';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {NgxTrimDirectiveModule} from "ngx-trim-directive";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatMomentDateModule} from "@angular/material-moment-adapter";
import {MAT_DATE_LOCALE} from "@angular/material/core";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatSelectModule} from "@angular/material/select";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {HttpService} from "src/app/services/http.service";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {HeaderInterceptor} from "src/app/interceptors/header-interceptor";
import {MatTooltipModule} from "@angular/material/tooltip";
import {UtilsService} from "src/app/utils/utils.service";
import { NewsLineComponent } from './components/news/news-line/news-line.component';
import {MatIconModule} from "@angular/material/icon";
import {DataService} from "src/app/services/data.service";
import {MatCheckboxModule} from "@angular/material/checkbox";
import { ContinueCancelDialogComponent } from './dialogs/continue-cancel-dialog/continue-cancel-dialog.component';
import {MatDialogModule} from "@angular/material/dialog";
import { HtmlSnackbarComponent } from './components/html-snackbar/html-snackbar.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    InputFormComponent,
    NewsComponent,
    MainLayoutComponent,
    NavigationComponent,
    FooterComponent,
    HeaderComponent,
    NewsLineComponent,
    ContinueCancelDialogComponent,
    HtmlSnackbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatInputModule,
    NgxTrimDirectiveModule,
    MatDatepickerModule,
    MatMomentDateModule,
    FlexLayoutModule,
    MatSelectModule,
    HttpClientModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatIconModule,
    MatCheckboxModule,
    MatDialogModule
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-AU' },
    HttpService,
    UtilsService,
    DataService,
    {provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
